drop table if exists bovag.rdw;

select
	trim(r.postcode_numeriek || r.postcode_alfanumeriek) as postcode,
	r.huisnummer::int as huisnummerint,
	*
	into bovag.rdw
from import.rdw r;

-- select count(*) from import.rdw;

create index on bovag.rdw (postcode);
create index on bovag.rdw (huisnummerint);

-- select * from bovag.rdw r where r.huisnummer_toevoeging = '';


/* match rdw records with bag. Using levenshtein on toevoeging */
drop table if exists bovag.rdw_levenshtein_matched;
select
    --matched_id,
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	geopunt,
	match_score
into bovag.rdw_levenshtein_matched
from (
	select  b.huisnummer, b.huisletter, b.huisnummertoevoeging,
			r.volgnummer,
			r.gevelnaam,
			r.naam_bedrijf,
			r.straat,
			r.plaats,
			r.api_bedrijf_erkenningen,
			--b.matched_id,
			b.pid,
			b.numid,
			b.vid,
			b.sid,
			b.lid,
			b.oppervlakte,
			b.geopunt,
			b.postcode,
			r.huisnummer || ' ' || huisnummer_toevoeging as rdw_toevoeging,
	        levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), lower(huisnummer_toevoeging)) match_score
	--from bovag.rdw_not_direct_matched r,
	from bovag.rdw r,
	lateral ( select
				--coalesce (pid, sid, lid) as matched_id,
				pa.pid,
				pa.numid,
				pa.vid,
				pa.sid,
				pa.lid,
				pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
				pa.postcode,
				pa.oppervlakte,
				pa.geopunt
	          from baghelp.all_pvsl_nums pa
	          where pa.postcode = r.postcode
	          and pa.huisnummer = r.huisnummerint
	          order by levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(r.huisnummer_toevoeging)) limit 1
	) b
	--where r.postcode is not null and r.huisnummerint is not null and r.huisnummer_toevoeging != ''
) p;
--where matched_id is not null;

select 'levenshtein matched 1', count(*) from bovag.rdw_levenshtein_matched rlm;

/*
 * Not Matched with levenshtein.
 */
drop table if exists bovag.rdw_nobaghit;

select r.*
into bovag.rdw_nobaghit
from bovag.rdw r
left outer join bovag.rdw_levenshtein_matched r2 on (r.huisnummerint=r2.huisnummer and r.postcode=r2.postcode)
where r2.huisnummer is null;


drop table if exists bovag.rdw_levenshtein_matched_no_bag;
/* now match nearby addresses  ~ 899 */
select
    --matched_id,
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	geopunt,
	match_score
into bovag.rdw_levenshtein_matched_no_bag
from (
	select  b.huisnummer, b.huisletter, b.huisnummertoevoeging,
			r.volgnummer,
			r.gevelnaam,
			r.naam_bedrijf,
			r.straat,
			r.plaats,
			r.api_bedrijf_erkenningen,
			--b.matched_id,
			b.pid,
			b.numid,
			b.vid,
			b.sid,
			b.lid,
			b.oppervlakte,
			b.geopunt,
			b.postcode,
			r.huisnummer || ' ' || huisnummer_toevoeging as rdw_toevoeging,
	        levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), lower(huisnummer_toevoeging)) + 10 match_score
	from bovag.rdw_nobaghit r,
	lateral ( SELECT * FROM (
				select * from (
				select
				--coalesce (pid, sid, lid) as matched_id,
				pa.pid,
				pa.numid,
				pa.vid,
				pa.sid,
				pa.lid,
				pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
				pa.postcode,
				pa.oppervlakte,
				pa.geopunt
	          from baghelp.all_pvsl_nums pa
	          where pa.postcode = r.postcode
	          and pa.huisnummer < r.huisnummerint
	          order by pa.huisnummer desc limit 1-- levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(r.huisnummer_toevoeging)) limit 1
			  ) lowerhuisnummer
			  UNION
			  SELECT * FROM (
				select
				--coalesce (pid, sid, lid) as matched_id,
				pa.pid,
				pa.numid,
				pa.vid,
				pa.sid,
				pa.lid,
				pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
				pa.postcode,
				pa.oppervlakte,
				pa.geopunt
	          from baghelp.all_pvsl_nums pa
	          where pa.postcode = r.postcode
	          and pa.huisnummer > r.huisnummerint
	          order by pa.huisnummer asc limit 1-- levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(r.huisnummer_toevoeging)) limit 1
			  ) higherhuisnummer
		limit 1) nearbyadres
	) b
) x;


drop table if exists bovag.rdw_bag_matched_new;

/* use union to combine to sql queries. bonus, removes duplicates. */
select
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	st_transform(geopunt, 3857) geometry,
	match_score
into bovag.rdw_bag_matched_new
from (
select * from bovag.rdw_levenshtein_matched
union
select * from bovag.rdw_levenshtein_matched_no_bag
) allmatches;


create index on bovag.rdw_bag_matched_new using gist(geometry);
alter table bovag.rdw_bag_matched_new alter column geometry type geometry(point, 3857);
ALTER TABLE bovag.rdw_bag_matched_new ADD COLUMN id SERIAL PRIMARY KEY;

-- replace old table with new one.
DROP table if exists bovag.rdw_bag_matched;
ALTER table bovag.rdw_bag_matched_new RENAME TO rdw_bag_matched;


select count(*), 'import rdw' as check from import.rdw;
select count(*), 'bag matched' as check from bovag.rdw_bag_matched;
select count(*), 'levenshtein matched' as check  from bovag.rdw_levenshtein_matched rlm;
select count(*), 'levenshtein no bag matched' as check from bovag.rdw_levenshtein_matched_no_bag rlmnb;
select count(*), 'rdw bag matched' as check from bovag.rdw_bag_matched rbm;
select count(*), 'match score > 1' as check from bovag.rdw_levenshtein_matched rlm where rlm.match_score > 1;

--comment on table bovag.rdw_bag_matched is '2023-10';

DO $do$BEGIN EXECUTE 'COMMENT ON TABLE bovag.rdw_bag_matched IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;



/*
 *
 * Analyze / test dataset.
 */


-- select * from baghelp.all_pvsl_nums apn where postcode = '1822BT' and huisnummer=58;
-- select * from baghelp.all_pvsl_nums apb where postcode = '9983PM';

-- select * from bagv2_032022.nummeraanduidingactueelbestaand n where postcode = '1822BT' and huisnummer = 58;
-- vacuum bovag.rdw;

-- select * from bovag.rdw202310 r where r.huisnummer_toevoeging != '';

-- select count(*) from bovag.rdw_levenshtein_matched rlm;
-- select count(*) from baghelp.all_pvsl_nums;
-- select count(*) from bagv2_062022.nummeraanduiding n where postcode = '5628RE' ;

/*
 *  Data selection exports.
 */
drop table if exists bovag.rdw_lambda;

select
        id,
	vid,  -- verblijfsobject identificatie
	pid,  -- pand identitficatie
	sid,  -- standplaats identificatie
	lid,  -- ligplaats identificatie
	numid,
	postcode,
	huisnummer,
	huisletter,
	huisnummertoevoeging,
	rdw_toevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	-- r.huisnummer_toevoeging,
	volgnummer,
	gevelnaam,
	naam_bedrijf,
	straat,
	plaats,
	api_bedrijf_erkenningen,
	oppervlakte,
	st_transform(geometry, 4326) geometry,
	match_score
into bovag.rdw_lambda
from bovag.rdw_bag_matched rbm;

DO $do$BEGIN EXECUTE 'COMMENT ON TABLE bovag.rdw_lambda IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;
select count(*), 'rdw_lambda' as check from bovag.rdw_lambda;


