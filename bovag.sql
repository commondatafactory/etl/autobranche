/* match bovag garages with BAG using leveshtein */
drop table if exists bovag.garages_new;
select
	a.pid,
	a.vid,
	a.sid,
	a.lid,
	a.numid,
	a.huisnummer, a.huisletter, a.huisnummertoevoeging,
	a.postcode,
	st_transform(a.geopunt, 3857) bagpunt,
	a.oppervlakte,
	b."memberId" as lidid,
	-- address,
	match_score,
	-- substring(address from '[0-9]+.*$') as bovag_toevoeging,
	concat(a.huisnummer, ' ', lower(a.huisletter) , lower(a.huisnummertoevoeging)) bag_toevoeging,
        b.slug,
	website as url,
	name,
	address->>'city' as bovag_plaats,
	address->>'postalCode' as bovag_postcode,
	concat(address->>'street', ' ', address->>'housenumber', ' ', address->>'numberAddition') as bovag_adres,
	st_transform(st_setsrid(st_point((b._geoloc->>'lng')::float, (b._geoloc->>'lat')::float), 4326), 3857) as geometry
into bovag.garages_new
from bovag.bovag_df b,
lateral ( select
		pa.pid,
		pa.numid,
		pa.vid,
		pa.sid,
		pa.lid,
		pa.huisnummer, pa.huisletter, pa.huisnummertoevoeging,
		pa.postcode,
		pa.oppervlakte,
		levenshtein(
                    --bag
                    trim(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging))),
                    --bovag
                    trim(lower(concat(address->>'housenumber', ' ', address->>'numberAddition')))
                ) as match_score,
		pa.geopunt
	        from baghelp.all_pvsl_nums pa
	        where pa.postcode = replace(b.address->>'postalCode', ' ', '')
	        -- and pa.huisnummer = r.huisnummerint
	        order by levenshtein(
	        	--bag
	        	trim(concat(pa.huisnummer, ' ', lower(pa.huisletter) , lower(pa.huisnummertoevoeging))),
	        	--bovag
	        	trim(lower(concat(address->>'housenumber', ' ', address->>'numberAddition')))
	        )  asc
	        limit 1
) a;

-- select _geoloc, (_geoloc->>'lat')::float lat, address->>'city' stad, trim(lower(concat(address->>'housenumber', ' ', address->>'numberAddition'))) huisnummer_tv  from bovag."import.bovag_df" ibd;

-- select * from bovag.bovag_df ibd;

-- create indexes
CREATE index on bovag.garages_new using gist (geometry);
ALTER table bovag.garages_new ALTER column geometry type geometry(point, 3857);
ALTER TABLE bovag.garages_new ADD COLUMN id SERIAL PRIMARY KEY;

-- replace old table with new one.
DROP table if exists bovag.garages;
ALTER table bovag.garages_new RENAME TO garages;

DO $do$BEGIN EXECUTE 'COMMENT ON TABLE bovag.garages IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;


/*
 * Dataselection export bovag.
 */
drop table if exists bovag.garages_lambda;
select
	pid,
	vid,
	sid,
	lid,
	numid,
	huisnummer, huisletter, huisnummertoevoeging,
	postcode,
	oppervlakte,
	lidid,
	-- address,
	match_score,
	-- bovag_toevoeging,
	bag_toevoeging,
	-- telephone,
	-- emailaddress,
        slug,
	url,
	name,
	bovag_plaats,
	bovag_postcode,
	bovag_adres,
	st_transform(bagpunt, 4326) as geometry
into bovag.garages_lambda
from bovag.garages;

DO $do$BEGIN EXECUTE 'COMMENT ON TABLE bovag.garages_lambda IS ''created: ' || to_char(LOCALTIMESTAMP, 'YYYY-MM-DD HH:MI:SS') || ''''; END $do$;

select current_date;
