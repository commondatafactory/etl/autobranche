""" Download algolia objects from bovag"""

from algoliasearch.search_client import SearchClient

# Use an API key with `browse` ACL
client = SearchClient.create("C2PG678GKC", "463a2bea7a3cdd13ed725a3f5d390436")
index = client.init_index("bovag-members")

# Get all records as iterator
# for record in index.browse_objects():
#     print(record)
res = index.search('', {
    'hitsPerPage': 9000,
})

# print(res)
# print(res['hits'])

for item in res['hits']:
    print(item)

print(len(res['hits']))
