FROM registry.gitlab.com/commondatafactory/dockerimages/importimage:latest

COPY gemeenten202403.csv /app/
COPY *.sh /app/
COPY *.sql /app/
COPY *.py /app/
COPY *.txt /app/

WORKDIR /app/

RUN mkdir -p rawdata/bovag
RUN mkdir -p rawdata/rdw

RUN pip install -r requirements.txt
