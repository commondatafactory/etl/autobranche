#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE="${PGDATABASE:-cdf}"
export PGHOST="${PGHOST:-127.0.0.1}"
export PGPORT="${PGPORT:-5432}"
export PGUSER="${PGUSER:-cdf}"
export PGPASSWORD="${PGPASSWORD:-insecure}"


filename="rawdata/bovag/rdw.csv"

if [[ $(find "$filename" -mtime -1 -print) ]]; then
  echo "File $filename exists and is at most 1 day old"
else
  echo "No recent $filename exists or is older 1 days, download again"
  curl -s "https://opendata.rdw.nl/api/views/5k74-3jha/rows.csv?accessType=DOWNLOAD" > $filename
fi

psql -c "DROP TABLE IF EXISTS import.rdw"
# import csv into postgres. coupling to bag information is needed to get geometric data.
pgfutter --ignore-errors=0 --ssl --host "${PGHOST}" --port "${PGPORT}" --dbname ${PGDATABASE} --username ${PGUSER} --pass ${PGPASSWORD} csv $filename

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

psql -f "rdw.sql" -v ON_ERROR_STOP=1
