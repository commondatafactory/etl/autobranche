Import Autobranche data from the Netherlands.
=============================================

 - Assumes postgres installation.

 - Needs a database table in `baghelp.all_pvsl_nums`
   which contains all dutch addresses of Pand, Vbo, Stanplaats, Ligplaat, Address infromation

configured with the following default postgres environment variables:

  PGDATABASE
  PGHOST
  PGPORT
  PGUSER
  PGPASSWORD


./imsport.sh
- download bovag garages
- downooad rdw garages
# for both RDW and BOVAG.
- import into database
- cleanup data and combine with BAG dataset

# finally
- Publish new cleaned tables used in map service (pgtilesever)
  with correct geo index and correct coordinate system.


# development.

- create virtual python environment.
- pip instal -r requirments.txt

Bovag_search.py is generated by using:

    jupyter nbconvert --to python bovag_search.ipynb

