#!/bin/bash
set -u
set -e
set -x

# Set connection Variables
export PGDATABASE="${PGDATABASE:-cdf}"
export PGHOST="${PGHOST:-127.0.0.1}"
export PGPORT="${PGPORT:-5432}"
export PGUSER="${PGUSER:-cdf}"
export PGPASSWORD="${PGPASSWORD:-insecure}"

# BOVAG.
python bovag_search.py

psql -f "bovag.sql" -v ON_ERROR_STOP=1
