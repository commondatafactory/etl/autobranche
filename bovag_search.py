#!/usr/bin/env python
# coding: utf-8

# In[74]:

import os
import csv
import sqlalchemy

from sqlalchemy import create_engine
import pandas as pd


from algoliasearch.search_client import SearchClient

# Use an API key with `browse` ACL
client = SearchClient.create("C2PG678GKC", "463a2bea7a3cdd13ed725a3f5d390436")
index = client.init_index("bovag-members")


# In[76]:


# lost of locations in the netherlands to geosearch
# will find all garagses. around range is highes possible.
locations = []
# made with QGIS.
with open('gemeenten202403.csv', encoding='utf8') as locs:
    for line in locs.readlines()[1:]:
        xyl = line.split(';')
        lng = float(xyl[0].replace(',', '.'))
        lat = float(xyl[1].replace(',', '.'))
        if len(xyl) >= 3:
            locations.append([lng, lat, xyl[2]])

# In[37]:


# example url
"""
aroundLatLng=52.3675734%2C4.9041389&aroundRadius=30000
&clickAnalytics=true&facets=%5B%22brands.value%22%2C%22services.value%22%2C%22vehicles.value%22%5D
&highlightPostTag=__%2Fais-highlight__&highlightPreTag=__ais-highlight__&hitsPerPage=9
&maxValuesPerFacet=200&page=0&tagFilters=&userToken=anonymous-7d3090db-214a-46fd-9c72-8623e09b285c
"""


all_hits = []

for loc in locations:
    lat = loc[1]
    lng = loc[0]
    # aroundLatLng
    res = index.search('', {
      'hitsPerPage': 1000,
      'aroundLatLng': f"{lat},{lng}",
      'aroundRadius': 1000000,
      # 'offset': page * 1000,
    })

    # print(res["hits"])
    hits = len(res["hits"])
    print(f" hits {hits} page {loc[2]}")

    if len(res["hits"]) >= 0:
        all_hits.extend(res["hits"])
        len(all_hits)
    else:
        continue

    res = None

print(len(all_hits))


# In[75]:


# find all unque objects.
unique_objects = {o['objectID']: o for o in all_hits}
print(len(unique_objects))


# In[40]:


# create csv to show in Qgis.


with open('rawdata/bovag_agolia.csv', 'w', encoding='utf8') as csvfile:
    writer = csv.writer(csvfile, delimiter=';')
    rows = []
    rows.append(['kvk', 'memberid', 'name', 'url', 'lat', 'lng'])
    for b in unique_objects.values():
        row = [
            b['kvkNumber'], b['memberId'],
            b['name'],
            b['website'],
            b['_geoloc']['lat'], b['_geoloc']['lng']]
        rows.append(row)
    writer.writerows(rows)
    print('created csv for inspection / debugging')


# In[68]:


df = pd.DataFrame.from_dict(unique_objects, orient='index')
# make json columns.
print(df.count())


# In[72]:


# store bovag data into database.

host = os.environ.get('PGHOST', 'localhost')
port = os.environ.get('PGPORT', '5433')
user = os.environ.get('PGUSER', 'cdf')
password = os.environ.get('PGPASSWORD', 'insecure')
database = os.environ.get('PGDATABASE', 'cdf')

e = create_engine(f'postgresql://{user}:{password}@{host}:{port}/{database}')

df.to_sql(
    'bovag_df', e,
    if_exists='replace',
    schema='bovag',
    dtype={
        "image": sqlalchemy.types.JSON,
        "vehicles": sqlalchemy.types.JSON,
        "address": sqlalchemy.types.JSON,
        "_geoloc": sqlalchemy.types.JSON,
        "brands": sqlalchemy.types.JSON,
        "services": sqlalchemy.types.JSON,
        "_highlightResult": sqlalchemy.types.JSON,
        "passthruBrands": sqlalchemy.types.JSON,
    },
)
